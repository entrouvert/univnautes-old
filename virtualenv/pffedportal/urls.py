from django.conf.urls import *
from django.views.generic.base import RedirectView
from django.views.generic import TemplateView

from django.conf import settings

urlpatterns = patterns('',
    url(r'^$', 'base.views.index'),
    url(r'^redirect$', 'base.views.redirect302'),
    url(r'^login$', 'base.views.login'),
    url(r'^sso$', 'base.views.sso'),
    url(r'^logout$', 'base.views.logout', name='auth_logout'),
    (r'^authsaml2/', include('authentic2.authsaml2.urls')),
)

urlpatterns += patterns('django.views.generic.simple',
    (r'^licences$', TemplateView.as_view(template_name='licences.html')),
    (r'^conditions$', TemplateView.as_view(template_name='conditions.html')),
)

if settings.EMAIL_RCPT and settings.EMAIL_HOST:
    urlpatterns += patterns('', url(r'^mail$', 'base.views.mail'))

if settings.DEBUG:
    from django.contrib import admin
    admin.autodiscover()
    urlpatterns += patterns('',
        (r'^admin/', include(admin.site.urls)),
    )

# fallback: go to index page
urlpatterns += patterns('',
    url(r'^.*$', RedirectView.as_view(url='/')),
    )

