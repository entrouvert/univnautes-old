/* Copied from http://paulgueller.com/2011/04/26/parse-the-querystring-with-jquery/
 * Modification: added decoding of parameters value using unescape()
 * Usage:
 * var qs = jQuery.parseQuerystring();
 * //qs['foo'] == "bar"
 * //qs.somevalue == "myvalue"
 */
jQuery.extend({
  parseQuerystring: function(){
    var nvpair = {};
    var qs = window.location.search.replace('?', '');
    var pairs = qs.split('&');
    $.each(pairs, function(i, v){
      var pair = v.split('=');
      nvpair[pair[0]] = unescape(pair[1]);
    });
    return nvpair;
  }
});
