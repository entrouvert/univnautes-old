Simple IdP Discovery Store and Query
====================================

How to use
----------

Copy this code in your web app.

Place the response.html somewhere, let's say at http://sp.example.com/response.html.

Add this:

  <script type="text/javascript" src="jquery-1.7.2.min.js"></script>
  <script type="text/javascript" src="jquery.qs.js"></script>
  <script type="text/javascript" src="discoquery.js"></script>
  <script type="text/javascript" src="debug.js"></script>

And make the linked files accessible.

Call this to get the globally stored preference:

  $.disco_query("http://sp.example.com/metadata", "http://disco.example.com/store", "http://sp.example.com/response.html", function (entity_id, auth) {
   // do something with it
 });

Call this to store your preference:

  $.disco_query("http://sp.example.com/metadata", "http://chosen_idp.example.com/metadata", "http://disco.example.com/store", "http://sp.example.com/response.html");
