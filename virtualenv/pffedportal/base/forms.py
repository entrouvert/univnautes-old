# -*- encoding: utf-8 -*-

from django import forms
from django.conf import settings
from authentic2.saml.common import get_idp_list_sorted

EMAIL_SUBJECTS_CHOICES = [ (u'%s' % i, settings.EMAIL_SUBJECTS[i]) for i in range(len(settings.EMAIL_SUBJECTS)) ]

IDP_CHOICES = [ (idp['entity_id'], idp['name']) for idp in get_idp_list_sorted() ]
IDP_CHOICES.insert(0, (None, "Sélectionner dans la liste"))

class MailForm(forms.Form):
    name = forms.CharField(label=u'Vos nom et prénom',
            max_length=200, required=True)
    from_email = forms.EmailField(label=u'Votre adresse électronique (mail)',
            max_length=150, required=True)
    idp = forms.ChoiceField(label=u"Votre établissement d'origine",
            choices=IDP_CHOICES, required=True)
    phone = forms.CharField(label=u'Un numéro de téléphone pour vous joindre (optionnel)',
            max_length=30, required=False)
    subject = forms.ChoiceField(label=u'Votre souci',
            choices=EMAIL_SUBJECTS_CHOICES, required=True)
    body = forms.CharField(label=u'La description détaillée de votre souci',
            max_length=5000, widget=forms.Textarea, required=True,
            help_text="""Précisez votre établissement d'origine, le type
            d'appareil que vous utilisez pour vous connecter, votre logiciel de
            navigation sur le web, etc.""")

    def __init__(self, *args, **kwargs):
        super(MailForm, self).__init__(*args, **kwargs)
        for f in self.fields:
            self.fields[f].widget.attrs['class'] = 'span8'

