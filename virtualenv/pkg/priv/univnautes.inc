<?php
/*
 * privileges for univnautes IdP
 *
 */

global $priv_list;

$priv_list['univnautes-idp'] = array();
$priv_list['univnautes-idp']['name'] = "UnivNautes IdP - account";
$priv_list['univnautes-idp']['descr'] = "Local IdP account";
$priv_list['univnautes-idp']['match'] = array();
$priv_list['univnautes-idp']['match'][] = "NOTHING";

$priv_list['univnautes-idp-multiple'] = array();
$priv_list['univnautes-idp-multiple']['name'] = "UnivNautes IdP - multiple connections";
$priv_list['univnautes-idp-multiple']['descr'] = "allowing multiple connections";
$priv_list['univnautes-idp-multiple']['match'] = array();
$priv_list['univnautes-idp-multiple']['match'][] = "NOTHING";

$priv_list['univnautes-idp-admin'] = array();
$priv_list['univnautes-idp-admin']['name'] = "UnivNautes IdP - administrator";
$priv_list['univnautes-idp-admin']['descr'] = "manage local IdP accounts";
$priv_list['univnautes-idp-admin']['match'] = array();
$priv_list['univnautes-idp-admin']['match'][] = "NOTHING";

?>
