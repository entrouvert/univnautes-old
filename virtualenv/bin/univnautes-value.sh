#!/bin/sh

# This script aims to be called by bsnmpd via "extCommand" lines

mdurl() {
	. /usr/local/univnautes/etc/univnautes.conf
	echo $MDURL
}

wlurl() {
	. /usr/local/univnautes/etc/univnautes.conf
	echo $WLURL
}

defaultidp() {
	. /usr/local/univnautes/etc/univnautes.conf
	echo $DEFAULTIDP
}

__idp() {
	sqlite3 /usr/local/univnautes/pffedportal/pffedportal.db "select count(entity_id) from saml_libertyprovider where federation_source='$1'"
}

idp_federation() {
	__idp federation
}

idp_local() {
	__idp local
}

ip_whitelist() {
	ipfw table 42 list | wc -l | tr -dc 0-9
	echo ""
}

cp_sessions() {
	cat /var/db/captiveportal.db | wc -l | tr -dc 0-9
	echo ""
}

$*

