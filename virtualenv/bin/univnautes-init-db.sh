#!/bin/sh

cd /usr/local/univnautes
. bin/activate

# build new database with django "syncdb"
for db in pffedportal pfidp
do
	cd /usr/local/univnautes/${db}
	rm -f ${db}.db
	python manage.py syncdb --noinput --traceback
	echo "${db} database created"
	python manage.py collectstatic --noinput --traceback --link
	echo "${db} collectstatic done"
done

echo "add metadata"
/usr/local/bin/univnautes-update-metadata.sh
/usr/local/bin/univnautes-update-local-metadata.sh

echo "build map info"
/usr/local/bin/univnautes-update-map.sh

echo "univnautes databases are ready"

exit 0
