#!/bin/sh

# clean out old data from the database (sessions)

cd /usr/local/univnautes
. bin/activate

cd pffedportal
python ./cleanup_sessions.py

# Cf https://docs.djangoproject.com/en/dev/ref/django-admin/#cleanup
cd ..
cd pfidp
python ./manage.py cleanup

exit 0
