#!/bin/sh

# restart bad tcpdump (memory leak...?)
/bin/pgrep -af 'tcpdump -s 256 -v -S -l -n -e -ttt -i pflog0' > /tmp/tcpdump.pid
if [ $? -eq 0 ]
then
	/sbin/conscontrol mute on 2>&1 > /dev/null
	kill `cat /tmp/tcpdump.pid` || exit 1
	sleep 1
	/usr/sbin/tcpdump -s 256 -v -S -l -n -e -ttt -i pflog0 2>&1 | logger -t pf -p local0.info 2> /dev/null &
	/sbin/conscontrol mute off 2>&1 > /dev/null
fi

exit 0
