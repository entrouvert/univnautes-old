#!/bin/sh

# config
. /usr/local/univnautes/etc/univnautes.conf

# If there is no local metadata, just stop...
if [ ! -r "$MDLOCAL" ]
then
	logger -p local4.info -t update-local-metadata "no local metadata"
	exit 1
fi

# virtualenv activation 
VIRTUAL_ENV="/usr/local/univnautes"
export VIRTUAL_ENV
PATH="$VIRTUAL_ENV/bin:$PATH"
export PATH

# Insert local metadata in portal database
cd /usr/local/univnautes/pffedportal
if [ -r pffedportal.db ]
then
	python ./manage.py sync-metadata --source="local" --idp --verbosity=2 $MDLOCAL 2>&1 | logger -p local4.info -t update-local-metadata
	/usr/local/bin/univnautes-update-map.sh
fi

# Insert local metadata in idp database
cd /usr/local/univnautes/pfidp
if [ -r pfidp.db ]
then
	python ./manage.py sync-metadata --source="local" --sp --verbosity=2 $MDLOCAL 2>&1 | logger -p local4.info -t update-local-metadata-idp
fi

