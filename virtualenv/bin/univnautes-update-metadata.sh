#!/bin/sh

# lock to avoid concurrent updates
LOCK=/var/run/univnautes-update-metadata_in-progress.lock
if [ -r $LOCK ]
then
	PID=`cat $LOCK`
	ps waux | grep $PID | grep univnautes | grep -vq grep && exit
fi
unlock() {
	rm -f $LOCK
	exit
}
trap unlock INT TERM EXIT
echo $$ > $LOCK

# config
. /usr/local/univnautes/etc/univnautes.conf

log() {
	logger -p local4.info -t update-metadata -- "$*"
}

# clean 
cd `dirname $MD`
rm -f $MD
cd `dirname $MDSP`
rm -f $MDSP

#
# 1. Download throught HTTPS
#

if [ -n "$MDURL" ]
then
	FETCH=$MD.fetch.$$

	if [ -r "$MDCA" ]
	then
		log "downloading IdPs metadata from $MDURL (ca=$MDCA)"
		wget --quiet --timeout=300 --ca-certificate=$MDCA -O $FETCH $MDURL
		RET=$?
	else
		log "downloading IdPs metadata from $MDURL (no-check-certificate)"
		wget --quiet --timeout=300 --no-check-certificate -O $FETCH $MDURL
		RET=$?
	fi

	if [ $RET -ne 0 ]
	then
		rm -f $FETCH
		log "error while downloading IdPs metadata (wget)"
		unset MD
		unset MDCRT
	else
		mv -f $FETCH $MD
	fi

else
	log "WARNING: no IdPs metadata url, use an empty one"
	MD=/usr/local/univnautes/etc/empty-metadata.xml
	unset MDCRT
fi

if [ -n "$MDSPURL" ]
then
	FETCH=$MDSP.fetch.$$

	if [ -r "$MDSPCA" ]
	then
		log "downloading SPs metadata from $MDSPURL (ca=$MDSPCA)"
		wget --quiet --timeout=300 --ca-certificate=$MDSPCA -O $FETCH $MDSPURL
		RET=$?
	else
		log "downloading SPs metadata from $MDSPURL (no-check-certificate)"
		wget --quiet --timeout=300 --no-check-certificate -O $FETCH $MDSPURL
		RET=$?
	fi

	if [ $RET -ne 0 ]
	then
		rm -f $FETCH
		log "error while downloading SPs metadata (wget)"
		unset MDSP
		unset MDSPCRT
	else
		mv -f $FETCH $MDSP
	fi
else
	log "WARNING: no SPs metadata url: use IdPs metadata url for SPs metadata url"
	MDSP=${MD}
	unset MDSPCRT
fi


#
# 2. Check metadatas
#

if [ -n "$MD" -a -r "$MDCRT" ]
then
	xmlsec1 --verify --pubkey-cert-pem $MDCRT $MD > /var/tmp/metadata-idps-xmlsec1.out 2>&1

	if [ $? -ne 0 ]
	then
		log "error while checking signature of IdPs metadata (xmlsec1) :"
		cat /var/tmp/metadata-idps-xmlsec1.out | logger -p local4.error -t update-idps-metadata.xmlsec1
		unset MD
	fi
elif [ -n "$MD" ]
then
	log "WARNING: do not check signature of IdPs metadata"
fi

if [ -n "$MDSP" -a -r "$MDSPCRT" ]
then
	xmlsec1 --verify --pubkey-cert-pem $MDSPCRT $MDSP > /var/tmp/metadata-sps-xmlsec1.out 2>&1

	if [ $? -ne 0 ]
	then
		log "error while checking signature of SPs metadata (xmlsec1) :"
		cat /var/tmp/metadata-sps-xmlsec1.out | logger -p local4.error -t update-sps-metadata.xmlsec1
		unset MDSP
	fi
elif [ -n "$MDSP" ]
then
	log "WARNING: do not check signature of SPs metadata"
fi


#
# 3. Insert metadata in portal (IdPs) and local idp (SPs) databases
#

# virtualenv activation 
VIRTUAL_ENV="/usr/local/univnautes"
export VIRTUAL_ENV
PATH="$VIRTUAL_ENV/bin:$PATH"
export PATH

# update in pffedportal
cd /usr/local/univnautes/pffedportal
if [ -r pffedportal.db ]
then
	if [ -n "$MD" ]
	then
		python ./manage.py sync-metadata --source="federation" --idp --verbosity=1 $MD 2>&1 | logger -p local4.info -t update-metadata
	fi
	/usr/local/bin/univnautes-update-map.sh
fi

# update in idp
cd /usr/local/univnautes/pfidp
if [ -r pfidp.db ]
then
	if [ -n "$MDSP" ]
	then
		python ./manage.py sync-metadata --source="federation" --sp --verbosity=1 $MDSP 2>&1 | logger -p local4.info -t update-metadata-idp
	fi
fi

exit 0

