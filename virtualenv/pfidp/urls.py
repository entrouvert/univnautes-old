from django.conf.urls.defaults import *
from django.contrib.auth.decorators import login_required
import settings
import authentic2.idp.views

urlpatterns = patterns('',
    url(r'^', include('authentic2.auth2_auth.urls')),
    url(r'^logout$', 'authentic2.idp.views.logout', name='auth_logout'),
    url(r'^idp/', include('authentic2.idp.urls')),
    url(r'^users-admin/', include('users_admin.urls')),
    url(r'^$', login_required(authentic2.idp.views.homepage), {}, 'index'),
)

if settings.DEBUG:
    from django.contrib import admin
    admin.autodiscover()
    urlpatterns += patterns('',
        (r'^admin/', include(admin.site.urls)),
    )

