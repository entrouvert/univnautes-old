from django.conf.urls.defaults import patterns
from django.contrib.auth.decorators import login_required

import views

urlpatterns = patterns('',
    (r'^$', login_required(views.index)),
    (r'^create$', login_required(views.create)),
    (r'^read/(?P<name>[a-z0-9\.\-_]+)$', login_required(views.read)),
    (r'^update/(?P<name>[a-z0-9\.\-_]+)$', login_required(views.update)),
    (r'^delete/(?P<name>[a-z0-9\.\-_]+)$', login_required(views.delete)),
    (r'^desactivate/(?P<name>[a-z0-9\.\-_]+)$', login_required(views.desactivate)),
    (r'^activate/(?P<name>[a-z0-9\.\-_]+)$', login_required(views.activate)),
    (r'^multiple$', login_required(views.multiple)),
    (r'^import$', login_required(views.csv_import)),
)

