#!/usr/bin/python
from django.core.management import execute_manager
try:
    import settings # Assumed to be in the same directory.
except ImportError:
    import sys
    sys.stderr.write("Error: Can't find the file 'settings.py' in the directory containing %r. It appears you've customized things.\nYou'll have to run django-admin.py, passing it your settings module.\n(If the file settings.py does indeed exist, it's causing an ImportError somehow.)\n" % __file__)
    sys.exit(1)

# logging system

import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
if settings.LOG_DEBUG:
    from logging.handlers import RotatingFileHandler
    handler = RotatingFileHandler(filename='/var/log/authentic_idp.log', maxBytes=1024*1024, backupCount=3)
    formatter = logging.Formatter('%(asctime)s authentic_idp: [%(levelname)s] %(name)s.%(message)s','%Y-%m-%d %H:%M:%S')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

# start !

if __name__ == "__main__":
    execute_manager(settings)

