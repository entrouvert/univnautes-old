#!/bin/sh

/usr/local/bin/php -q << EOPHP
<?php

require("util.inc");
require("notices.inc");
require("config.lib.inc");
require("univnautes.inc");

log_error("create/update /var/lib/univnautes from config.xml");
\$config=parse_config();
univnautes_sync();
univnautes_texts_build();

?>
EOPHP
