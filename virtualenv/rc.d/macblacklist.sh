#!/bin/sh

. /usr/local/univnautes/etc/univnautes.conf

log() {
	        logger -p local4.info -t macblacklist -- "$*"
}

case "$1" in
	stop) echo "if you really wanna delete blacklist : ipfw delete 666";;
	*)
		/sbin/ipfw -fq delete 666
		if ! test -r "$MACBL"
		then
			log "no MAC blacklist (MACFILE=$MACFILE is not readable)"
			exit 0
		fi
		log "reset MAC blacklist from $MACBL"
		# don't consider comments
		sed 's/#.*$//' $MACBL | while read MAC
		do
			# sanitize
			MAC=`echo $MAC | tr -dc 0-9a-fA-F:`
			if test -n "$MAC"
			then
				if /sbin/ipfw -fq add 666 deny MAC $MAC any
				then
					log "added: $MAC"
				else
					log "ERROR can't add: $MAC (incorrect MAC address ?)"
				fi
			fi
		done
		;;
esac

exit 0

