#!/bin/sh

. /usr/local/univnautes/etc/univnautes.conf
UPDATER=/usr/local/bin/univnautes-update-metadata.sh
MAPUPDATER=/usr/local/bin/univnautes-update-map.sh
CLEANUP=/usr/local/bin/univnautes-cleanup.sh
TCPDUMPRESTART=/usr/local/bin/univnautes-tcpdump-restart.sh

log() {
	logger -p local4.info -t univnautes-rc.d -- "$*"
}

start() {
	_prestart
	_start_portal
	_start_idp
	_cronstart
}

stop() {
	_cronstop
	_stop_idp
	_stop_portal
}

restart() {
	_cronstop
	_stop_idp
	_start_portal
	_start_idp
	_cronstart
}

_prestart() {
	# if we launch pffedportal after an update
	if [ -x /usr/local/univnautes/bin/postupdate ]
	then
		log "exec postupdate script"
		mv -f /usr/local/univnautes/bin/postupdate \
			/usr/local/univnautes/bin/postupdate.done
		/usr/local/univnautes/bin/postupdate.done
	fi
	# create databases if one is missing
	if [ ! -f /usr/local/univnautes/pffedportal/pffedportal.db \
		-o ! -f /usr/local/univnautes/pfidp/pfidp.db ]
	then
		log "initialize databases"
		/usr/local/univnautes/bin/univnautes-init-db.sh
	fi
}

_start_portal() {
	_stop_portal # kill pfsense legacy captive portal lighttpd, if any
	log "pffedportal: starting..."
	cd /usr/local/univnautes
	# virtualenv activation
	. bin/activate
	cd pffedportal
	log "pffedportal: start FCGI workers"
	python manage.py runfcgi socket=/tmp/pffedportal.sock method=prefork daemonize=true pidfile=/var/run/pffedportal-fcgi.pid
	cd ..
	# install lighttp config and start lighttp
	log "pffedportal: start lighttpd frontend"
	cp -f etc/lighttpd-pffedportal.conf /var/etc/lighty-CaptivePortal-SSL.conf
	rm -f /var/etc/lighty-CaptivePortal.conf # should not exist, but... pfsense magic...
	lighttpd -f /var/etc/lighty-CaptivePortal-SSL.conf
	log "pffedportal: started"
}

_stop_portal() {
	# kill all pffedportal processes
	for f in `ls /var/run/pffedportal-fcgi.pid /var/run/lighty-CaptivePortal*pid 2> /dev/null`
	do
		log "pffedportal: stopping - kill `basename $f .pid`"
		kill `cat $f`
	done
	log "ppfedportal: stopped"
}

_start_idp() {
	if [ "x${ENABLEIDP}" != "xon" ]
	then
		log "pfidp: disabled"
		return
	fi
	log "pfidp: starting..."
	cd /usr/local/univnautes
	# virtualenv activation
	. bin/activate
	cd pfidp
	log "pfidp: start FCGI workers"
	python manage.py runfcgi socket=/tmp/pfidp.sock method=prefork daemonize=true pidfile=/var/run/pfidp-fcgi.pid
	cd ..
	# install lighttp config and restart lighttp 
	log "pfidp: start lighttpd frontend"
	lighttpd -f etc/lighttpd-pfidp.conf
	log "pfidp: started"
}

_stop_idp() {
	# kill all pfidp processes
	for f in `ls /var/run/pfidp-fcgi.pid /var/run/lighty-pfidp.pid 2> /dev/null`
	do
		log "pfidp: stopping - kill `basename $f .pid`"
		kill `cat $f`
	done
	log "pfidp: stopped"
}

_cronstart() {
	_cronstop
	/usr/local/bin/minicron $REFRESH /var/run/update-metadata-cron.pid $UPDATER
	log "update-metadata cron started (every $REFRESH seconds)"
	/usr/local/bin/minicron $REFRESH /var/run/update-map-cron.pid $MAPUPDATER
	log "update-map cron started (every $REFRESH seconds)"
	/usr/local/bin/minicron 300 /var/run/cleanup-cron.pid $CLEANUP
	log "cleanup cron started (every 300 seconds)"
	/usr/local/bin/minicron 86400 /var/run/tcpdump-restart-cron.pid $TCPDUMPRESTART
	log "tcpdump-restart cron started (every 86400 seconds)"
}

_cronstop() {
	for cron in update-metadata update-map cleanup tcpdump-restart
	do
		if [ -r /var/run/${cron}-cron.pid ]
		then
			PID=`cat /var/run/${cron}-cron.pid`
			ps waux | grep "$PID" | grep minicron | grep -vq grep && kill $PID
			rm -f /var/run/${cron}-cron.pid
			log "${cron} cron stopped"
		fi
	done
}

$1

