#!/usr/local/bin/bash

LOCAL=/usr/local
VE=univnautes

BASE_DIR=/home/pfsense
BUILDER_TOOLS=$BASE_DIR/tools
BUILDER_SCRIPTS=$BUILDER_TOOLS/builder_scripts
BUILDER_PROFILES=$BUILDER_SCRIPTS/builder_profiles
UNIVNAUTES=$BUILDER_PROFILES/univnautes

echo "** building virtualenv in $LOCAL/$VE"

if [ -e $LOCAL/$VE ]
then
	echo "$0: $LOCAL/$VE already exists... using it..." 1>&2
	exit 0
fi

cd $LOCAL
virtualenv $VE

echo "** activate virtualenv"
cd $VE
. bin/activate

echo "** install packages"
for p in flup "django<1.6" authentic2
do
	pip install "$p"
done

for f in pffedportal pfidp bin etc rc.d pkg www
do
	echo "** create `pwd`/`basename $f`"
	cp -a $UNIVNAUTES/virtualenv/$f .
done

echo "** populate www directory"
touch www/index.html
mkdir www/media
ln -s /var/db/cpelements www/media/local

echo "*** manually add lasso and sqlite3 modules"
cp -av /usr/local/lib/python2.7/site-packages/*lasso* ./lib/python2.7/site-packages/
cp -av /usr/local/lib/python2.7/sqlite3 ./lib/python2.7/

echo "** cleaning pyc/pyo"
find . -name "*.pyc" -exec rm {} \;
find . -name "*.pyo" -exec rm {} \;
cd $LOCAL
virtualenv --relocatable $VE

cd $VE
echo "** $LOCAL/$VE is ready"
