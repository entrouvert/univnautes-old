PF_HOME = /home/pfsense/
PF_PROFILES = $(PF_HOME)/tools/builder_scripts/builder_profiles/
PF_FS = /usr/local/pfsense-fs

.sinclude "Makefile.local"

all:
	@echo ""
	@echo "  make ve     rebuilt univnautes virtualenv"
	@echo "  make iso    build ISO, IMG, updates"
	@echo "  make clean  clean build dirs -- WARNING: next build will be very long (1 hour min)"
	@echo "  make pull   pull pfSense git clones"
	@echo ""

ve: pull
	cd builder && ./build_virtualenv.sh

iso: pull
	cd builder && ./build.sh

clean:
	cd builder && ./clean.sh

pull:
	cd builder && ./git_pull_all.sh

