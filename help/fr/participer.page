<page xmlns="http://projectmallard.org/1.0/"
      type="topic" style="task"
      id="participer" xml:lang="fr">

<info>
  <link type="guide" xref="index" group="participer"/>
  <revision pkgversion="20120605" version="1.0" date="10-06-2012" status="final"/>
  <credit type="author">
    <name>Thomas Noël</name>
    <email>tnoël@entrouvert.com</email>
  </credit>
  <credit type="maintainer">
    <name>Pierre Cros</name>
    <email>pcros@entrouvert.com</email>
  </credit>
</info>

<title>Participer au développement et aux tests d'UnivNautes</title>

<section id="tests">
  <title>Versions de tests</title>
  <p>De temps à autres, des versions de test d'UnivNautes sont disponibles et
  annoncées sur la liste de discussion du projet. Voici comment participer aux
  tests !
  </p>

  <section id="tests-iso">
  <title>Test des images ISO</title>
  <p>Les images ISO de test sont sur <link
  href="http://isos.univnautes.entrouvert.com/test/">http://isos.univnautes.entrouvert.com/test/</link>.
  La procédure d'installation est identique à celle des images ISO d'UnivNautes
  classiques.</p>
  </section>

  <section id="tests-upgrade">
  <title>Test des mises à jour</title>
  <p>Pour mettre à jour une installation existante d'Univnautes vers une version de test :
  </p>
  <steps>
  <item>
  <p>Faire une sauvegarde de la machine actuelle (idéalement, clonage de la machine virtuelle ; sinon au moins copie du fichier <code>/conf/config.xml</code>)</p>
  </item>
  <item>
  <p>Mettre à jour avec la version à tester :</p>
  <steps>
  <item><p>Aller dans « System / Firwmare », onglet « Updater Settings »</p></item>
  <item><p>Au niveau de « Default Auto Update URLs », choisir la source « TEST Univnautes Updates (Entr'ouvert) »</p></item>
  <item><p>Cliquer sur le bouton [Save] en bas de page pour enregistrer la modification</p></item>
  <item><p>Revenir sur la page d'accueil qui proposera une nouvelle version,
  lancer l'upgrade tel que proposé (ou aller « System/Firmware », onglet « Auto
  Update »)</p></item>
  </steps>
  </item>
  <item>
  <p>À la fin de l'upgrade, rebooter... (c'est automatique, en fait)</p>
  </item>
  <item>
  <p><em>En cas d'upgrade en 2012 : aller dans « System/General Setup » et
  indiquer le bon fuseau horaire dans « Time zone » : Europe/Paris, puis
  cliquer sur [Save] en bas de page</em></p>
  </item>
  <item>
  <p>Vérifier que le service fonctionne toujours...</p>
  </item>
  </steps>
  <p>... et laisser tourner quelques jours !</p>
  </section>

  <section id="tests-problemes">
  <title>En cas de soucis pendant le test</title>
  <p>Bien détailler le problème et copier tous les fichiers de
  <code>/var/log/</code> pour analyse <em>post-mortem</em> par un des
  développeurs.</p>
  </section>
</section>

<section id="devs">
  <title>Développement du projet</title>
<p>
UnivNautes est basé sur le système libre <link href="http://pfsense.org/">pfSense</link>.
</p>

  <p>
  Pour jouer avec le système UnivNautes, il faut avoir quelques connaissances
  basiques en PHP et/ou Python et/ou shell script et/ou SAML... et
  éventuellement connaître un peu les principes de pfSense et FreeBSD.
  </p>
  <p>
  Le site de gestion du projet est ici : <link
  href="http://dev.entrouvert.org/projects/portail-captif">http://dev.entrouvert.org/projects/portail-captif</link>.
  En plus du dépôt contenant les codes sources, vous y trouverez les
  informations nécessaires pour entrer en contact avec l'équipe du projet
  (UNPIdF) et les développeurs (principalement chez Entr'ouvert à l'heure
  actuelle).
  </p>
</section>

</page>
