<page xmlns="http://projectmallard.org/1.0/"
      type="topic" style="task"
      id="install-deroulement" xml:lang="fr">

<info>
<link type="guide" xref="installation" group="install-deroulement"/>
  <desc>Différentes étapes de l'installation.</desc>
  <revision pkgversion="20120605" version="1.0" date="10-06-2012" status="final"/>
  <credit type="author">
    <name>Thomas Noël</name>
    <email>tnoël@entrouvert.com</email>
  </credit>
  <credit type="maintainer">
    <name>Pierre Cros</name>
    <email>pcros@entrouvert.com</email>
  </credit>
</info>

<title>Déroulement de l'installation</title>

<p>L'installation se déroule en quelques étapes. Comme tout le système est pré-configuré, il suffit la plupart du temps de sélectionner les choix par défaut. Ci-dessous les écrans qui apparaîtront et les choix à effectuer.</p>

<figure>
  <title>Écran 1</title>
  <desc>Propose de choisir le type d'écran, la police d'affichage et le clavier. Mais cela n'a aucun impact réel sur la suite, on peut donc accepter directement les choix par défaut.</desc>
  <media type="image" mime="image/png" src="figures/install1.png" >
  </media>
</figure>

<figure>
  <title>Écran 2</title>
  <desc>Nous allons faire une installation rapide. Une partition BSD va occuper tout le disque dur de la machine et le système va y être copié intégralement :</desc>
  <media type="image" mime="image/png" src="figures/install2.png" >
  </media>
</figure>

<figure>
  <title>Écran 3</title>
  <desc>Dernière chance d'annulation avant l'effacement du disque dur de la machine...</desc>
  <media type="image" mime="image/png" src="figures/install3.png" >
  </media>
</figure>

<figure>
  <title>Écran 4</title>
  <desc>L'installation prend quelques secondes sur une machine rapide (quelques minutes sur une machine virtuelle KVM sur mon portable personnel).</desc>
  <media type="image" mime="image/png" src="figures/install5.png" >
  </media>
</figure>

<figure>
  <title>Écran 5</title>
  <desc>Choix du noyau en fonction du processeur : le choix SMP convient toujours, même sur une machine à un seul coeur.</desc>
  <media type="image" mime="image/png" src="figures/install6.png" >
  </media>
</figure>

<figure>
  <title>Écran 6</title>
  <desc>L'installation est terminée... On va rebooter pour démarrer la machine sur le portail captif.</desc>
  <media type="image" mime="image/png" src="figures/install7.png" >
  </media>
</figure>

<figure>
  <title>Reboot...</title>
  <desc>La machine redémarre. Il faut retirer le périphérique d'installation (cédérom) à cet instant.</desc>
  <media type="image" mime="image/png" src="figures/install8.png" >
  </media>
</figure>

<note style="warning">
<p>
Finition sur une machine virtuelle VMware : sur VMware, il a été signalé un bogue avec l'émulation CDROM dans le noyau proposé dans FreeBSD 8.1 (la plateforme de pfSense 2.0 sur laquelle repose Univnautes). En conséquence, après installation, il faut retirer le périphérique CDROM de la machine virtuelle.
</p>
</note>


</page>
