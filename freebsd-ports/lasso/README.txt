#
# preparation d'un tar.gz de "lasso-2.3.99" (version git HEAD)
#

cd ~
wget http://repos.entrouvert.org/lasso.git/snapshot/lasso-master.tar.gz
tar zxf lasso-master.tar.gz
mv lasso-master lasso-2.3.99
cd lasso-2.3.99
./autogen.sh noconfig
cd ..
tar zcvf lasso-2.3.99.tar.gz lasso-2.3.99
cp lasso-2.3.99.tar.gz /usr/ports/distfiles/

# indication du fichier .tar.gz dans le port
md5 lasso-2.3.99.tar.gz >> /usr/ports/security/lasso/distinfo
sha256 lasso-2.3.99.tar.gz >> /usr/ports/security/lasso/distinfo
#... et revoir /usr/ports/security/lasso/distinfo a la main

#
# retour dans le ports : univnautes/freebsd-ports/lasso/
#

cd /root/univnautes/freebsd-ports/lasso/

# verification des dependances
make depends

# configuration de la compilation -- y activer uniquement python
make config

# construction des binaires
make

# installation du port
make install

# construction du paquet (.tbz, inutile finalement)
# make package

