#!/bin/sh

cd /home/pfsense/tools/builder_scripts/builder_profiles/univnautes && pwd && git pull

echo ""
cd /home/pfsense/tools && pwd && git pull
echo "create remove.list.iso.8 (don't remove zoneinfo)"
grep -v "/zoneinfo/" /home/pfsense/tools/builder_scripts/conf/rmlist/remove.list.iso.8 | grep -v "/usr/src" > /home/pfsense/tools/builder_scripts/builder_profiles/univnautes/remove.list.iso.8

echo ""
cd /home/pfsense/freesbie2 && pwd && git pull

echo ""
if [ ! -d /home/pfsense/pfSenseGITREPO/pfSenseGITREPO ]; then
	mkdir -p /home/pfsense/pfSenseGITREPO
	cd /home/pfsense/pfSenseGITREPO
	rm -rf pfSenseGITREPO
	pwd
	git clone git://github.com/pfsense/pfsense.git -b RELENG_2_0 pfSenseGITREPO
else
	cd /home/pfsense/pfSenseGITREPO/pfSenseGITREPO && pwd && git pull
fi

