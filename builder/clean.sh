#!/bin/sh

TOOLSDIR=/home/pfsense/tools/builder_scripts/
export TOOLSDIR

# copy "univnautes" pfSense configuration (pfsense-build.conf) in TOOLSDIR
cp pfsense-build.conf $TOOLSDIR

# configure build environment (pfSense scripts)
cd $TOOLSDIR
. ./pfsense_local.sh

# Ensure $SRCDIR exists
mkdir -p $SRCDIR

./clean_build.sh

echo ">>> delete virtualenv /usr/local/univnautes"
rm -rf /usr/local/univnautes

