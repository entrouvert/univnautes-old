#!/bin/sh

TOOLSDIR=/home/pfsense/tools/builder_scripts/
export TOOLSDIR

# copy "univnautes" pfSense configuration (pfsense-build.conf) in TOOLSDIR
cp pfsense-build.conf $TOOLSDIR

# configure build environment (pfSense scripts)
cd $TOOLSDIR
. ./pfsense_local.sh

# Ensure $SRCDIR exists
mkdir -p $SRCDIR

# univnautes virtualenv build
rm -rf /usr/local/univnautes
./builder_profiles/univnautes/virtualenv/build_virtualenv.sh

# install UN virtualenv in copy_overlay
echo ">>> Clean copy_overlay : delete univnautes virtualenv"
rm -rf $custom_overlay/usr/local/univnautes
mkdir -p $custom_overlay/usr/local
echo ">>> Copy univnautes virtualenv (/usr/local/univnautes) in copy_overlay"
cp -a /usr/local/univnautes $custom_overlay/usr/local

