#!/bin/sh

TOOLSDIR=/home/pfsense/tools/builder_scripts/
export TOOLSDIR

# copy "univnautes" pfSense configuration (pfsense-build.conf) in TOOLSDIR
cp pfsense-build.conf $TOOLSDIR

# configure build environment (pfSense scripts)
cd $TOOLSDIR
. ./pfsense_local.sh

# indicates UnivNautes version in /etc/version
UN_VERSION=${PFSENSE_VERSION}"+UNIVNAUTES-"`date +%Y%m%d-%H%M`
echo $UN_VERSION > "${custom_overlay}/etc/version"

# Ensure $SRCDIR exists
mkdir -p $SRCDIR

# Start building, really (pfSense scripts)
#./update_git_repos.sh
#./build_pfPorts.sh
$TOOLSDIR/builder_profiles/univnautes/builder/build_updates.sh
$TOOLSDIR/builder_profiles/univnautes/builder/build_iso.sh

